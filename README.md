# contentis-frontend

The ~~soon to be~~ app that lets you easily choose food that fits your lifstyle.

## Building

- Download and install [Flutter](https://flutter.dev/) along with [Android studio](https://developer.android.com/studio/index.html#downloads)
- Make sure you're on ~~version 1.7.8 (`flutter version 1.7.8+hotfix.4`)~~ any recent version of flutter is fine, actually
- Connect your android device (iDevices should work, but are untested due to lack of iDevices), make sure it has usb debugging enabled, and do `flutter run release`

## Libraries used

- barcode_scan (modified), gives us a nice EAN/QR/Whatever scanner.
- http, does.. internet things, name says it all honestly.
- image_picker, allows us to pick images easily.
- shared_preferences, for easy saving of user preferences.
- transparent_image, as a placeholder image somewhere.
- package_info, for showing the version number

## Todo

All done :D