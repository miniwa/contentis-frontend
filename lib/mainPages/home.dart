import 'package:contentis_frontend/app_localizations.dart';
import 'package:contentis_frontend/errorWidget.dart';
import 'package:contentis_frontend/secondaryPages/itemDetail.dart';
import 'package:contentis_frontend/globals.dart' as globals;
import 'package:contentis_frontend/styles.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, 'home_title')),
      ),
      body: FutureBuilder(
        future: http.get("https://jacwib.pythonanywhere.com/api/0.1/top-item/" + globals.prefs.getString("veganoption").toString() ?? "Meat"), //honestly, you tell me on this one.
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Animate.fadeSwitcher(Container(
                child: errorWidget(),
              ));
            } else {
              var _aa = convert.jsonDecode(snapshot.data.body);
              return Animate.fadeSwitcher(Container(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  itemCount: _aa.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: _aa[index.toString()]["title"] == "hideme" ? null : () {
                        Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ItemDetail(_aa[index.toString()]["title"], _aa[index.toString()]["ean"].toString())),
                      );},
                      child: Card(
                        color: Colors.grey,
                        margin: EdgeInsets.all(8),
                        child: Stack(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              child: Image.network(
                                  _aa[index.toString()]["image-front"]), //tthankS PYTHON VERY COOL >>>>>>>>>>>>>>>>>>int.toString because JSON
                            ),
                            _aa[index.toString()]["title"] == "hideme" ? Container() : Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                color: Color.fromRGBO(0, 0, 0, 0.5),
                              ),
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                _aa[index.toString()]["title"],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                ),
                              ),
                            )
                          ],
                          alignment: AlignmentDirectional.bottomCenter,
                        ),
                      ),
                    );
                  },
                ),
              ));
            }
          } else {
            return Animate.fadeSwitcher(
                Center(child: CircularProgressIndicator()));
          }
        },
      ),
    );
  }
}
