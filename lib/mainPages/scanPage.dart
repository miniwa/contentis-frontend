import 'package:contentis_frontend/secondaryPages/itemDetail.dart';
import 'package:contentis_frontend/secondaryPages/addItem.dart';
import 'package:contentis_frontend/showError.dart';
import 'package:contentis_frontend/styles.dart';
import 'package:contentis_frontend/app_localizations.dart';
import 'package:contentis_frontend/globals.dart' as globals;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert' as convert;
import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:barcode_scan/barcode_scan.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  static const _kFontFam = 'font'; //import our magic font with the cool icons
  static const IconData barcodeIcon =
      const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData nbarcodeIcon = const IconData(0xe802,
      fontFamily:
          _kFontFam);

  //For the scan button
  //List that hold the scanned codes
  List apiResponse = [];

  List _eans = [];

  bool scanning = false;

  Future _future;

  Future _loadLists() async {
    List _savedResponses = [];
    List _savedEans = [];
    try {
      _savedResponses = convert
              .jsonDecode(globals.prefs.getString("responses")) ??
          []; // the part after = basically means "get the list, if its null (doesnt exist) return []"
      _savedEans = convert.jsonDecode(globals.prefs.getString("eans")) ?? [];
    } on NoSuchMethodError {
      _savedResponses = [];
      _savedEans = [];
    }

    if (mounted == true) {
      setState(() {
        apiResponse =
            _savedResponses; //here we... you know, actually set the list to the list
        _eans = _savedEans;
      });
    }
  }

  Future _saveLists() async {
    globals.prefs
        .setString("responses", convert.jsonEncode(apiResponse).toString());
    globals.prefs.setString("eans", convert.jsonEncode(_eans).toString());
  }

  Future requestData(String barcode) async {
    var _url =
        "https://jacwib.pythonanywhere.com/api/0.1/fetch-item/" + barcode;
    var _response = await http.get(_url);
    if (_response.statusCode == 200) {
      //barcode finns i databas :D
      var _jsonResponse = convert.jsonDecode(_response.body);
      return _jsonResponse;
    } else {
      return barcode;
    }
  }

  Future _scan() async {
    try {
      String _scannedBarcode = await BarcodeScanner.scan();
      setState(() {
        scanning = true;
      });
      try {
        var _response = await requestData(_scannedBarcode);
        if (_response == _scannedBarcode) {
          //item not in db :c
          setState(() {
            //apiResponse.remove(_scannedBarcode);
            apiResponse.insert(0, _scannedBarcode);
            //_eans.remove(_scanbarcode);
            _eans.insert(0, _scannedBarcode);
            scanning = false;
            _saveLists();
          });
        } else {
          //item in db :D
          setState(() {
            //apiResponse.remove(_response);
            apiResponse.insert(0, _response);
            //_eans.remove(_scannedBarcode);
            _eans.insert(0, _scannedBarcode);
            scanning = false;
            _saveLists();
          });
        }
      } catch (e) {
        if (mounted == true) {
          showError(context, 'Unknown network error: $e');
        }
      }
    }
    // Error when Camera access is denied
    on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        showError(context, 'The user did not grant the camera permission!');
      }
      // When PlatformException other than "camera access denied" is encountered
      else {
        showError(context, 'Unknown error: $e');
      }
    }
    // Error when nothing is scanned and user exits
    on FormatException {
      //we're just catching this, dont do shit
    }
    // When other errors other than above is encountered
    catch (e) {
      if (mounted == true) {
        showError(context, 'Unknown camera error: $e');
      }
    }
  }

  void _navigateToAddItem(
      BuildContext context, String ean, String mode, int index) async {
    final _result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AddItem(ean, "create", index: index)),
    );
    if (_result != null) {
      setState(() {
        scanning = true;
      });
      var _response = await requestData(_eans[index]);
      setState(() {
        if (_response != ean && _response != null) {
          apiResponse[index] = _response;
          _saveLists();
        }
        scanning = false;
        _saveLists();
      });
    }
  }

  void _navigateToItemDetail(BuildContext context, String item, String ean,
      String apiResponse, int index) {
    if (ean == apiResponse) {
      SnackBar _aaa = SnackBar(
        content:
            Text(translate(context, 'scanpage_notindb')),
        action: SnackBarAction(
          label:
              translate(context, 'scanpage_notindbbutton'),
          onPressed: () {
            _navigateToAddItem(context, ean, "create", index);
          },
        ),
      );
      Scaffold.of(context).showSnackBar(_aaa);
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ItemDetail(item, ean)),
      );
    }
  }

  Widget _pleaseScanProducts() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
              opacity: 0.5,
              child: Icon(
                //muh dark them!!!1
                nbarcodeIcon,
                size: 100,
              )),
          SizedBox(height: 20),
          Text(
            translate(context, 'scanpage_pleasescan'),
            style: TextStyle(fontSize: 20, color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Color isGudForU(List vegan, int index) {
    String _veganoption = globals.prefs.getString("veganoption") ?? "Meat";
    bool _halal = globals.prefs.getBool("halal") ?? false;
    int _veganoptionint;
    switch (_veganoption) {
      case "Meat":
        _veganoptionint = 0;
        break;
      case "Vegetarian":
        _veganoptionint = 1;
        break;
      case "Pesketarian": // we dont handle this, yet...
        _veganoptionint = 1;
        break;
      case "Vegan":
        _veganoptionint = 2;
        break;
      default: 
        _veganoptionint = 0;
    }
    if (apiResponse[index] != _eans[index]) {
      int _yes;
      bool _yeslal;
      try {
        _yes = int.parse(apiResponse[index]['vegan']); //GAY LEGACY SHIT (this dumb try statement can be removed in prod)
      } catch (e) {
        _yes = apiResponse[index]['vegan'];
      }
      if (_halal == false) { //if user isnt halal, NBD, return true
        _yeslal = true;
      } else if (apiResponse[index]['allergies']['halal'] == null) { //if we dont know if the object is halal, return yellow.
        return Colors.yellow;
      } else {
        if (apiResponse[index]['allergies']['halal'] ?? false == true) { //if we do know, return either yes or no you get the idea.
          _yeslal = true;
        } else {
          _yeslal = false;
        }
      }
      return _yes >= _veganoptionint && _yeslal == true ? Colors.green : Colors.red; //if everything is good, return green, otherwise return red.
    } else {
      return Colors.yellow;
    }
  }

  Widget _scannedItemsList() {
    return ListView.separated(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
      itemCount: apiResponse.length,
      separatorBuilder: (context, index) => Divider(height: 0),
      itemBuilder: (BuildContext context, int index) {
        String itemBarcode = _eans[index];
        String itemTitle = _eans[index];
        String itemImage =
            "http://www.i2clipart.com/cliparts/6/a/4/b/clipart-icon-with-question-mark-128x128-6a4b.png";
        if (apiResponse[index] != _eans[index]) {
          itemTitle = apiResponse[index]['title'];
        }
        if (apiResponse[index] != _eans[index] &&
            apiResponse[index]['image-front'] != null) {
          itemImage = apiResponse[index]['image-front'];
        }
        return Container(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              stops: [0.0,0.5,1.0],
              colors: [
                Colors.transparent,
                Colors.transparent,
                isGudForU(apiResponse, index)
              ]
            ),
          ),
          child: ListTile(
            contentPadding: EdgeInsets.all(5),
            title: Text(itemTitle),
            leading: CircleAvatar(
              radius: 25.0,
              backgroundImage: NetworkImage(
                  itemImage), //consider replacing this with a https://pub.dev/documentation/cached_network_image/latest/?
            ),
            onTap: () {
              _navigateToItemDetail(context, itemBarcode, _eans[index],
                  apiResponse[index].toString(), index);
            },
          ),
        );
      },
    );
  }

  @override
  initState() {
    super.initState();
    _future = _loadLists();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, 'scanpage_title')),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            tooltip:
                translate(context, 'scanpage_trashcan'),
            onPressed: () {
              setState(() {
                apiResponse = [];
                _eans = [];
                _saveLists();
              });
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              scanning == false) {
            if (snapshot.hasError) {
              return Animate.fadeSwitcher(Container(
                child: Text("uh oh"),
                //child: Animate.fadeSwitcher(errorWidget()), //dunno how this would happen but why not i guess
              ));
            } else {
              if (apiResponse.isEmpty) {
                return Animate.fadeSwitcher(_pleaseScanProducts());
              } else {
                return Animate.fadeSwitcher(_scannedItemsList());
              }
            }
          } else {
            return Animate.fadeSwitcher(
                Center(child: CircularProgressIndicator()));
          }
        },
      ),

      // Floating action button that launches the scanning page when pressed
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(barcodeIcon),
        label: Text(translate(context, 'scanpage_button')),
        onPressed: _scan,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
