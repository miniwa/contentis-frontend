import 'package:contentis_frontend/secondaryPages/about.dart';
import 'package:flutter/material.dart';
import 'package:contentis_frontend/styles.dart';
import 'package:contentis_frontend/app_localizations.dart';
import 'package:contentis_frontend/globals.dart' as globals;

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool _darktheme;
  String _veganoption;
  String _language;
  bool _halal;

  static const IconData religious_islam = const IconData(0xe830, fontFamily: "font2");

  void setSettings(String pref, bool value) async {
    globals.prefs.setBool(pref, value);
  }

  @override void initState() {
    _darktheme = globals.prefs.getBool("darktheme") ?? false;
    _veganoption = globals.prefs.getString("veganoption") ?? "Meat";
    _language = globals.prefs.getString("language") ?? "Auto";
    _halal = globals.prefs.getBool("halal") ?? false;
    super.initState();
  }

  bool systemDarkOn() {
    return MediaQuery.of(context).platformBrightness.toString() ==
            Brightness.dark.toString()
        ? true
        : false;
  }

  Widget bodyWidget() {
      return Animate.fadeSwitcher(
        ListView(
          children: <Widget>[
            SwitchListTile(
              title: Text(translate(context, 'settings_darktheme')),
              value: systemDarkOn() ? true : _darktheme,
              onChanged: systemDarkOn() ? null : (bool value) {
                setState(() {
                  _darktheme = value;
                  globals.darkMode = _darktheme;
                  globals.prefs.setBool("darktheme", value);
                  themeBloc.changeTheme(value);
                });
              },
              secondary: const Icon(Icons.lightbulb_outline),
            ),
            ListTile(
              title: Text(translate(context, 'settings_vegan')),
              trailing: DropdownButton(
                value: _veganoption,
                onChanged: (String value) {
                  setState(() {
                    _veganoption = value;
                    globals.prefs.setString("veganoption", value);
                  });
                },
                items: <String>[
                      'Meat',
                      'Pesketarian',
                      'Vegetarian',
                      'Vegan'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
              ),
              leading: const Icon(Icons.fastfood),
            ),
            SwitchListTile(
              title: Text(translate(context, 'settings_absolutelyharam')),
              value: _halal,
              onChanged: (bool value) {
                setState(() {
                  _halal = value;
                  setSettings("halal", value);
                });
              },
              secondary: const Icon(religious_islam),
            ),
            ListTile(
              title: Text(translate(context, 'settings_language')),
              trailing: DropdownButton(
                value: _language,
                onChanged: (String value) {
                  setState(() {
                    _language = value;
                    globals.prefs.setString("language", value);
                  });
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate('settings_pleaserestart'))));
                },
                items: <String>[
                      'Auto',
                      'English',
                      'Svenska',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
              ),
              leading: const Icon(Icons.language),
            ),
            ListTile(
              title: Text(translate(context, 'settings_about')),
              leading: const Icon(Icons.info),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => About()),
                );
              },
            ),
          ],
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, 'settings_title')),
      ),
      body: Container(
        child: bodyWidget(),
      ),
    );
  }
}
