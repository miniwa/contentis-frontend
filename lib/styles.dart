import 'dart:async';
import 'package:flutter/material.dart';

class Styles {
  static const bottomNavBarItemStyle =
      TextStyle(fontSize: 15, fontWeight: FontWeight.bold);
  static final todoListDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(20),
    color: Colors.teal, //annars oläsbart på dark mode :)
  );
  static const todoListTitleStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white);
  static const todoListTextStyle = TextStyle(fontSize: 15, color: Colors.white);
}

class Animate {
  static fadeSwitcher(Widget _plum) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 200),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return FadeTransition(child: child, opacity: animation);
      },
      child: _plum,
    );
  }
}

class DarkBloc {
  final _themeController =
      StreamController<bool>(); //ignore this error, since the sink needs to stay open . we ll, always. until the app closes.
  get changeTheme => _themeController.sink.add;
  get darkThemeEnabled => _themeController.stream;
}

final themeBloc = DarkBloc();