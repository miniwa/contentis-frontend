import 'package:contentis_frontend/app_localizations.dart';
import 'package:contentis_frontend/application.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:contentis_frontend/globals.dart' as globals;
import 'package:contentis_frontend/styles.dart';
import 'dart:math';

// Only allows potrait mode
Future main() async {
  globals.prefs = await SharedPreferences.getInstance();
  if (globals.prefs.getString("uuid") == null) {
    var rng = new Random.secure();
    globals.prefs.setString("uuid", rng.nextInt(4294967296).toString()); //"jack, vi behöver ett kontosystem" "kan det vara anonymt?" "ok sure"
  }                                                                            //heres ur user account bro
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  bool them = globals.prefs.getBool("darktheme") ?? false;
  String language = globals.prefs.getString("language") ?? "Auto";
  runApp(Contentis(useDark: them, setLang: language));
}

class Contentis extends StatelessWidget {
  final bool useDark;
  final String setLang;

  Contentis({Key key, @required this.useDark, @required this.setLang}) : super(key: key);

  final ThemeData lightTheme = ThemeData(
    primarySwatch: Colors.teal,
    accentColor: Colors.pinkAccent,
    brightness: Brightness.light,
    buttonColor: Colors.teal,
  );

  final ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.teal,
    scaffoldBackgroundColor: Colors.black, //backgroubd color
    accentColor: Colors.pinkAccent,
    toggleableActiveColor: Colors.pinkAccent, //settings thing color
    appBarTheme: AppBarTheme(
      color: Colors.teal, //top bar color
    ),
    primaryColorDark: Colors.teal, //circleavatar color
    canvasColor: Colors.black, //bottomnavbar bgcolor
  );

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: themeBloc.darkThemeEnabled,
      initialData: useDark ? true : false,
      builder: (context, snapshot) {
        return MaterialApp(
          title: 'Contentis',
          theme: snapshot.data ? darkTheme : lightTheme,
          darkTheme: darkTheme,
          home: Application(),
          supportedLocales: [
            Locale('en', 'US'),
            Locale('sv', 'SE'),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          localeResolutionCallback: (locale, supportedLocales) {
            if (setLang != "Auto") {
              switch (setLang) {
                case "Svenska":
                  return Locale('sv', 'SE');
                  break;
                case "English":
                  return Locale('en', 'US');
                  break;
                default:
                  return supportedLocales.first;
                  break;
              }
            } else {
                for (var supportedLocale in supportedLocales) {
                  if (supportedLocale.languageCode == locale.languageCode &&
                    supportedLocale.countryCode == locale.countryCode) {
                  return supportedLocale;
                }
              }
              return supportedLocales.first;
            }
          },
        );
      }
    );
  }
}
