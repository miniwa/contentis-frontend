import 'package:flutter/material.dart';
import 'package:contentis_frontend/app_localizations.dart';
import 'package:contentis_frontend/styles.dart';
import 'package:contentis_frontend/mainPages/scanPage.dart';
import 'package:contentis_frontend/mainPages/home.dart';
import 'package:contentis_frontend/mainPages/settings.dart';

// Application or Landing Page
class Application extends StatefulWidget {
  Application({Key key}) : super(key: key);

  static final version = "1.0";
  static final stage = "debug";

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  int _theIndex = 1;

  final controller = PageController(
    initialPage: 1,
  );

  BottomNavigationBarItem bottomNavBarItem(
    BuildContext context, IconData icon, String title) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      title: Text(
        title,
        style: Styles.bottomNavBarItemStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: controller,
        children: [
          Settings(),
          Home(),
          ScanPage(),
        ],
        onPageChanged: _updateTheIndex,
      ),
      // The BottomNavigationBar
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          bottomNavBarItem(context, Icons.settings, translate(context, 'bottomappbar_settings')),
          bottomNavBarItem(context, Icons.home, translate(context, 'bottomappbar_home')),
          bottomNavBarItem(context, Icons.search, translate(context, 'bottomappbar_scans'))
        ],
        currentIndex: _theIndex,
        onTap: _onPageItemTapped,
      ),
    );
  }
  
  void _updateTheIndex(int index) {
    setState(() {
      _theIndex = index; 
    });
  }

  void _onPageItemTapped(int index) {
    setState(() {
      controller.animateToPage(
        index,
        duration: const Duration(milliseconds: 400),
        curve: Curves.easeInOut,
      );
      _theIndex = index;
    });
  }
}
