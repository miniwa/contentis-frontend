
class Item {
  final String barcode;
  final String facts;

  Item(this.barcode, this.facts);

  Item.fromJson(Map<String, String> json)
      : barcode = json['barcode'],
        facts = json['facts'];
}
