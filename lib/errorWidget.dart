import 'package:flutter/material.dart';

errorWidget() {
  return Container(
	child: Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon( //muh dark them!!!1
          Icons.warning,
          size: 100,
        ),
        SizedBox(height: 20),
        Text(
          "Something went wrong :(\nCheck your internet connection?",
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  ));
}