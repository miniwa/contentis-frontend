import 'package:flutter/material.dart';
import 'package:contentis_frontend/app_localizations.dart';
import 'package:package_info/package_info.dart';

class About extends StatefulWidget {
  const About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  PackageInfo packageInfo;

  String version = "";

  Widget bigText(String text) {
    return Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 8), child: Text(text, textAlign: TextAlign.center, style: TextStyle(fontSize: 24)));
  }

  Widget smolText(String text) {
    return Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 16), child: Text(text, style: TextStyle(fontSize: 16)));
  }
  
  loadThing() async {
    packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      version = "v" + packageInfo.version;
    });
  }


  @override
  void initState() {
    super.initState();
    loadThing();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, 'about_appbar')),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            bigText('Contentis'),
            smolText(translate(context, 'about_av') + "Jack Wiberg" + translate(context, 'about_och') + "Enayatullah Norozi"),
            smolText(version),
          ],
        ),
      ),
    );
  }
}