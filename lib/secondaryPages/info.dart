import 'package:flutter/material.dart';
import 'package:contentis_frontend/app_localizations.dart';

class Info extends StatelessWidget {
  const Info({Key key}) : super(key: key);

  Widget bigText(String text) {
    return Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 8), child: Text(text, textAlign: TextAlign.center, style: TextStyle(fontSize: 24)));
  }

  Widget smolText(String text) {
    return Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 16), child: Text(text, style: TextStyle(fontSize: 16)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, 'info_compare')),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            bigText(translate(context, 'info_eater')),
            smolText(translate(context, 'info_meat')),
            bigText(translate(context, 'vegetarian')),
            smolText(translate(context, 'info_vegetarian')),
            bigText(translate(context, 'pesketarian')),
            smolText(translate(context, 'info_pesketarian')),
            bigText(translate(context, 'vegan')),
            smolText(translate(context, 'info_vegan')),
            bigText("Halal"),
            smolText(translate(context, 'info_halal')),
          ],
        ),
      ),
    );
  }
}