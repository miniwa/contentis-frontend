import 'package:contentis_frontend/secondaryPages/info.dart';
import 'package:flutter/material.dart';
import 'package:contentis_frontend/secondaryPages/addItem.dart';
import 'package:contentis_frontend/errorWidget.dart';
import 'package:contentis_frontend/styles.dart';
import 'package:contentis_frontend/app_localizations.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:transparent_image/transparent_image.dart';

class ItemDetail extends StatefulWidget {
  final String item;
  final String ean;

  ItemDetail(this.item, this.ean);

  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  String _getTitle(_uwu) {
    return (_uwu['title'] ?? "error");
  }

  String _getImage(_uwu) {
    if (_uwu['image-front'] != null) {
      return (_uwu['image-front']);
    } else {
      return ("https://miniwa.space/error-image-generic.png");
    }
  }

  String _getDesc(_uwu) {
    if (_uwu['description'] != null) {
      return (_uwu['description']);
    } else {
      return translate(context, 'itemdetail_noinfo');
    }
  }

  String _getVegan(_uwu) {
    var _vegan = "??????";
    switch (_uwu['vegan']) {
      case 0:
        {
          _vegan = translate(context, 'notvegetarian');
        }
        break;
      case 1:
        {
          _vegan = translate(context, 'vegetarian');
        }
        break;
      case 2:
        {
          _vegan = translate(context, 'vegan');
        }
        break;
      default:
        {
          _vegan = "?????";
        }
        break;
    }
    return _vegan;
  }

  _bodyWidget(BuildContext context, String _title, String _image, String _vegan, String _desc) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Positioned(
          height: deviceHeight / 1.5,
          width: deviceWidth - 20,
          left: 10.0,
          top: deviceHeight * 0.1,
          child: Card(
            elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 20.0),
                Text(
                  _title,
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20.0),
                AspectRatio(
                  aspectRatio: 1 / 1,
                  child: FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: _image,
                    width: 512,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 20.0),
                Text(
                  _vegan,
                  style: TextStyle(fontSize: 20.0),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20.0),
                Text(
                  _desc,
                  style: TextStyle(fontSize: 20.0),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[400],
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.help),
              tooltip:
                  translate(context, 'itemdetail_help'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Info()),
                );
              }),
          IconButton(
              icon: Icon(Icons.edit),
              tooltip:
                  translate(context, 'itemdetail_edit'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AddItem(this.widget.ean, "edit")),
                );
              }),
        ],
      ),
      body: FutureBuilder(
        future: http.get(
            "https://jacwib.pythonanywhere.com/api/0.1/fetch-item/" +
                this.widget.ean),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Animate.fadeSwitcher(Container(
                child: errorWidget(),
              ));
            } else {
              var _aa = convert.jsonDecode(snapshot.data.body);
              return Animate.fadeSwitcher(Container(
                  child: _getTitle(_aa) == "error"
                      ? errorWidget()
                      : _bodyWidget(context, _getTitle(_aa), _getImage(_aa),
                          _getVegan(_aa), _getDesc(_aa))));
            }
          } else {
            return Animate.fadeSwitcher(
                Center(child: CircularProgressIndicator()));
          }
        },
      ),
    );
  }
}
