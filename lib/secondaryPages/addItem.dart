import 'package:contentis_frontend/errorWidget.dart';
import 'package:contentis_frontend/styles.dart';
import 'package:contentis_frontend/app_localizations.dart';

import 'dart:convert';
import 'dart:convert' as convert;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:transparent_image/transparent_image.dart';

class AddItem extends StatefulWidget {
  final String ean;
  final String mode;
  final int index;

  AddItem(this.ean, this.mode, {this.index});

  @override
  _AddItemState createState() => _AddItemState();
}

class _AddItemState extends State<AddItem> {
  final _formKey = GlobalKey<FormState>();
  String _veganValue;
  String _itemName;
  String _itemLink;
  bool _halalValue = false;
  bool _submitting = false;
  Color _textColor = Colors.white;
  Future _future;
  File _imageFile;
  bool _awshit = false;

  _displayImage() {
    if (_imageFile != null) {
      return Animate.fadeSwitcher(Image.file(_imageFile));
    } else if (_itemLink != "" && _itemLink != null) {
      return Animate.fadeSwitcher(FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: _itemLink,
      ));
    } else {
      //show placeholder imaggg
      return Animate.fadeSwitcher(Column(
        children: <Widget>[
          Icon(Icons.camera_enhance, size: 100, color: _textColor),
          Text(
            translate(context, 'additem_taptoaddimagehint'),
            style: TextStyle(color: _textColor),
          )
        ],
      ));
    }
  }

  Future _getImage() async {
    try {
      var _image = await ImagePicker.pickImage(source: ImageSource.camera);
      if (_image != null) {
        setState(() {
          _imageFile = _image;
        });
      }
    } on PlatformException {}
  }

  _submitForm(String mode) async {
    final FormState _form = _formKey.currentState;
    if (_form.validate()) {
      _form.save();
      setState(() {
        _submitting = true;
      });
      int _veganInt = 0;
      switch (_veganValue) {
        case "Meat":
          _veganInt = 0;
          break;
        case "Pesketarian":
          _veganInt = 0; //change this later
          break;
        case "Vegetarian":
          _veganInt = 1;
          break;
        case "Vegan":
          _veganInt = 2;
          break;
        default:
          _veganInt = 0;
          break;
      }
      var _response;
      var _url = "http://jacwib.pythonanywhere.com/api/0.1/upload-item";
      try {
        if (_imageFile != null) {
          _response = await http.post(_url,
              headers: {"Content-Type": "application/json"},
              body: convert.jsonEncode({
                'ean': this.widget.ean,
                'title': _itemName,
                'vegan': _veganInt,
                'image-file': base64Encode(_imageFile.readAsBytesSync()),
                'allergies': {'halal': _halalValue},
                'edit': mode == "create" ? "false" : "true",
              }));
        } else {
          _response = await http.post(_url,
              headers: {"Content-Type": "application/json"},
              body: convert.jsonEncode({
                'ean': this.widget.ean,
                'title': _itemName,
                'vegan': _veganInt,
                'image-front': _itemLink,
                'allergies': {'halal': _halalValue},
                'edit': mode == "create" ? "false" : "true",
              }));
        }
        if (_response.statusCode == 200) {
          Navigator.pop(context, this.widget.index);
        } else {
          setState(() {
            debugPrint(_response.body.toString());
            _awshit = true;
          });
        }
      } catch (e) {
        setState(() {
          debugPrint(e);
          _awshit = true; //double the error handling!!
        });
      }
    }
  }

  _bodyWidget(BuildContext context, String mode, String ean, String name,
      String image, String vegan) {
    if (image != "" && image != null) {
      _itemLink = image;
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: ListView(
        children: <Widget>[
          Text(
            translate(context, this.widget.mode == "edit" ? 'additem_titleedit' : 'additem_titleadd') + " " + this.widget.ean,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Text(AppLocalizations.of(context)
                    .translate('additem_productname')),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return AppLocalizations.of(context)
                          .translate('additem_productnameempty');
                    }
                    if (value == "error") {
                      return "lol edge case"; //lol
                    }
                    return null;
                  },
                  initialValue: name,
                  decoration: InputDecoration(
                      hintText: AppLocalizations.of(context)
                          .translate('additem_productnamehint')),
                  onSaved: (val) => _itemName = val,
                ),
                SizedBox(height: 20),
                Text(AppLocalizations.of(context)
                    .translate('additem_taptoaddimage')),
                GestureDetector(
                  child: _displayImage(),
                  onTap: _getImage,
                ),
                SizedBox(height: 20),
                Text(
                    AppLocalizations.of(context).translate('additem_veganbox')),
                DropdownButtonFormField(
                  value: _veganValue, //why tf is there no initialvalue for this??? well, sucks to be you if you're editing an item....
                  hint: Text(AppLocalizations.of(context)
                      .translate('additem_veganboxhint')),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return AppLocalizations.of(context)
                          .translate('additem_veganboxempty');
                    }
                    return null;
                  },
                  onChanged: (String changedValue) {
                    setState(() {
                      _veganValue = changedValue;
                    });
                  },
                  //onSaveeeeeeeeeeeeeehahahaha yeah, that one would be funny wouldent it?
                  items: <String>['Meat', 'Pesketarian', 'Vegetarian', 'Vegan']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                CheckboxListTile(
                  value: _halalValue,
                  onChanged: (bool changedValue) {
                    setState(() {
                      _halalValue = changedValue;
                    });
                  },
                  title: Text(translate(context, 'additem_halal')),
                ),
                RaisedButton(
                  onPressed: () {
                    if (!_submitting) {
                      if (_imageFile != null ||
                          (_itemLink != "" && _itemLink != null)) {
                        setState(() {
                          _textColor = Colors.white;
                        });
                        _submitForm(this.widget.mode);
                      } else {
                        try {
                          _formKey.currentState
                              .validate(); //validate the stupid form
                          setState(() {
                            _textColor = Colors.red;
                          });
                        } catch (NoSuchMethodError) {
                          //in case the user is too fast with pressing the fucking button
                        }
                      }
                    }
                  },
                  child: Text(
                      AppLocalizations.of(context).translate('additem_submit'),
                      style: TextStyle(
                          color: Colors
                              .white)), //OH NO HARDCODED TEXT COLOR but its fine cause it looks good on both dark and light theme
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  initState() {
    super.initState();
    _future = http.get(
        this.widget.mode == "edit" ? "https://jacwib.pythonanywhere.com/api/0.1/fetch-item/" +
            this.widget.ean : "https://jacwib.pythonanywhere.com/api/0.1/scrape-info/" +
            this.widget.ean);
  }

  @override
  Widget build(BuildContext context) {
    String _mode;
    if (this.widget.mode == "edit") {
      _mode = translate(context, 'additem_titleedit');
    } else {
      _mode = translate(context, 'additem_titleadd');
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(_mode),
      ),
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (_awshit == true) {
            return errorWidget();
          }
          if (snapshot.connectionState == ConnectionState.done &&
              _submitting == false) {
            if (snapshot.hasError) {
              return Animate.fadeSwitcher(Container(
                child: errorWidget(),
              ));
            } else {              
              var _aa = convert.jsonDecode(snapshot.data.body);
              return Animate.fadeSwitcher(Container(
                child: _bodyWidget(context, this.widget.mode, this.widget.ean,
                    _aa['title'] ?? null, _aa['image-front'] ?? null, null),
              ));
            }
          } else {
            return Animate.fadeSwitcher(
                Center(child: CircularProgressIndicator()));
          }
        },
      ),
    );
  }
}
